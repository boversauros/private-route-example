import {
  BrowserRouter as Router,
  Routes,
  Route,
  Link,
  useLocation,
  Navigate,
} from "react-router-dom";
import "./App.css";

function Accessible() {
  return (
    <>
      <h2>This is accessible</h2>
      <p>
        Access to private place{" "}
        <Link to="/not" state={{ allowNot: true }}>
          Click me
        </Link>
      </p>
    </>
  );
}

function NotAccessible() {
  let location = useLocation();
  if (!location.state?.allowNot) {
    return <Navigate to="/" state={{ from: location }} replace />;
  }
  return (
    <>
      <h2>This is not accessible</h2>
      <p>
        go back home <Link to="/">CLick me</Link>
      </p>
    </>
  );
}

function Home() {
  return (
    <p>
      go to the yes page <Link to="/yes">Click me</Link>
    </p>
  );
}

function App() {
  return (
    <Router>
      <div className="App">
        <h1>hello</h1>
        <Routes>
          <Route path="/" element={<Home />} />
          <Route path="yes" element={<Accessible />} />
          <Route path="not" element={<NotAccessible />} />
        </Routes>
      </div>
    </Router>
  );
}

export default App;
