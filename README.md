# Example private routes

This project is a basic example of explaining private and public routes. In this case, we can find three routes:

- Home: The main entrance page. From this one, we can navigate to the other public route of the project.
- Yes: This is the different public route of the project. From this one, we can navigate to the private route.
- No: Private route of the project. We can only access it through the Yes public page.

This example is using [React Router Dom v6](https://reactrouter.com/docs/en/v6)

## Scripts for the project

### `npm run dev`
A dev server that provides rich feature enhancements over native ES modules, for example extremely fast Hot Module Replacement (HMR).

### `npm run build`
A build command that bundles your code with Rollup, pre-configured to output highly optimized static assets for production.

### `npm run preview`
Locally preview production build

More information can be found in the offical vite docs [page](https://vitejs.dev/guide/#index-html-and-project-root)
